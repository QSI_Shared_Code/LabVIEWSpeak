﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="NI.Project.Description" Type="Str">
&lt;b&gt; &lt;/b&gt; &lt;b&gt; &lt;/b&gt;
LabVIEW Speak TNG © 2017 Q Software Innovations, LLC
written by Quentin "Q" Alldredge
produced in LabVIEW 2015

Based on LVSpeak 2.0 Distribution as created by Norm Kirchner.
Sounds and Graphics are based on or from Star Trek: The Next Generation or other Star Trek series.  Trademarks and copyright owned by CBS.  No money was made from the creation of this tool.

support@qsoftwareinnovations.com
www.qsoftwareinnovations.com

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    * Neither the name of Q Software Innovations, LLC. nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Command Sets" Type="Folder">
			<Item Name="Command Set.lvclass" Type="LVClass" URL="../Command Sets/Command Set/Command Set.lvclass"/>
			<Item Name="LabVIEW Align Commands.lvclass" Type="LVClass" URL="../Command Sets/LabVIEW Align Commands/LabVIEW Align Commands.lvclass"/>
			<Item Name="LabVIEW Basic Commands.lvclass" Type="LVClass" URL="../Command Sets/LabVIEW Basic Commands/LabVIEW Basic Commands.lvclass"/>
			<Item Name="LVSpeak Commands.lvclass" Type="LVClass" URL="../Command Sets/LVSpeak Commands/LVSpeak Commands.lvclass"/>
			<Item Name="QuickDrop Speak Block Diagram.lvclass" Type="LVClass" URL="../Command Sets/QuickDrop Speak Block Diagram/QuickDrop Speak Block Diagram.lvclass"/>
			<Item Name="QuickDrop Speak Front Panel.lvclass" Type="LVClass" URL="../Command Sets/QuickDrop Speak Front Panel/QuickDrop Speak Front Panel.lvclass"/>
			<Item Name="Windows Basic Commands.lvclass" Type="LVClass" URL="../Command Sets/Windows Basic Commands/Windows Basic Commands.lvclass"/>
		</Item>
		<Item Name="LVSpeak TNG" Type="Folder">
			<Item Name="Sounds" Type="Folder">
				<Item Name="A New Collection of Data.wav" Type="Document" URL="../Sounds/A New Collection of Data.wav"/>
				<Item Name="A Simulation Would Be Limited.wav" Type="Document" URL="../Sounds/A Simulation Would Be Limited.wav"/>
				<Item Name="Access Denied, Logs.wav" Type="Document" URL="../Sounds/Access Denied, Logs.wav"/>
				<Item Name="Access Denied.wav" Type="Document" URL="../Sounds/Access Denied.wav"/>
				<Item Name="Access is Restricted.wav" Type="Document" URL="../Sounds/Access is Restricted.wav"/>
				<Item Name="Accessing Database Module.wav" Type="Document" URL="../Sounds/Accessing Database Module.wav"/>
				<Item Name="Accessing File.wav" Type="Document" URL="../Sounds/Accessing File.wav"/>
				<Item Name="Accessing Library Computer Database.wav" Type="Document" URL="../Sounds/Accessing Library Computer Database.wav"/>
				<Item Name="Accessing.wav" Type="Document" URL="../Sounds/Accessing.wav"/>
				<Item Name="Acknowledged.wav" Type="Document" URL="../Sounds/Acknowledged.wav"/>
				<Item Name="Adjustments Complete.wav" Type="Document" URL="../Sounds/Adjustments Complete.wav"/>
				<Item Name="Affirmative.wav" Type="Document" URL="../Sounds/Affirmative.wav"/>
				<Item Name="Alert 01 .wav" Type="Document" URL="../Sounds/Alert 01 .wav"/>
				<Item Name="Alert 02 .wav" Type="Document" URL="../Sounds/Alert 02 .wav"/>
				<Item Name="Altering Search Parameters.wav" Type="Document" URL="../Sounds/Altering Search Parameters.wav"/>
				<Item Name="Anti-Matter Containment Positive.wav" Type="Document" URL="../Sounds/Anti-Matter Containment Positive.wav"/>
				<Item Name="asterisk.wav" Type="Document" URL="../Sounds/asterisk.wav"/>
				<Item Name="Authorization Denied.wav" Type="Document" URL="../Sounds/Authorization Denied.wav"/>
				<Item Name="Auto Defence Proc Initiated.wav" Type="Document" URL="../Sounds/Auto Defence Proc Initiated.wav"/>
				<Item Name="Auto Destruct Seq Armed.wav" Type="Document" URL="../Sounds/Auto Destruct Seq Armed.wav"/>
				<Item Name="Auto Engage Time Sequence.wav" Type="Document" URL="../Sounds/Auto Engage Time Sequence.wav"/>
				<Item Name="Awaiting Input.wav" Type="Document" URL="../Sounds/Awaiting Input.wav"/>
				<Item Name="By Accessing Available Imagery.wav" Type="Document" URL="../Sounds/By Accessing Available Imagery.wav"/>
				<Item Name="Cancelled.wav" Type="Document" URL="../Sounds/Cancelled.wav"/>
				<Item Name="closeprogram.wav" Type="Document" URL="../Sounds/closeprogram.wav"/>
				<Item Name="Command Codes Verified.wav" Type="Document" URL="../Sounds/Command Codes Verified.wav"/>
				<Item Name="Command Functions are Offline.wav" Type="Document" URL="../Sounds/Command Functions are Offline.wav"/>
				<Item Name="command.wav" Type="Document" URL="../Sounds/command.wav"/>
				<Item Name="Confirm Deactivation Request.wav" Type="Document" URL="../Sounds/Confirm Deactivation Request.wav"/>
				<Item Name="criticalstop.wav" Type="Document" URL="../Sounds/criticalstop.wav"/>
				<Item Name="Damage Alarm 01 .wav" Type="Document" URL="../Sounds/Damage Alarm 01 .wav"/>
				<Item Name="Damage Alarm 02.wav" Type="Document" URL="../Sounds/Damage Alarm 02.wav"/>
				<Item Name="Danger, Safety Limits.wav" Type="Document" URL="../Sounds/Danger, Safety Limits.wav"/>
				<Item Name="Deactivation Complete Short.wav" Type="Document" URL="../Sounds/Deactivation Complete Short.wav"/>
				<Item Name="Deactivation Complete.wav" Type="Document" URL="../Sounds/Deactivation Complete.wav"/>
				<Item Name="Deck One Life Support.wav" Type="Document" URL="../Sounds/Deck One Life Support.wav"/>
				<Item Name="defaultsound.wav" Type="Document" URL="../Sounds/defaultsound.wav"/>
				<Item Name="Define Parameters of Program.wav" Type="Document" URL="../Sounds/Define Parameters of Program.wav"/>
				<Item Name="Define, Please.wav" Type="Document" URL="../Sounds/Define, Please.wav"/>
				<Item Name="Deflector Shield Failure.wav" Type="Document" URL="../Sounds/Deflector Shield Failure.wav"/>
				<Item Name="Deny Beep 01 .wav" Type="Document" URL="../Sounds/Deny Beep 01 .wav"/>
				<Item Name="Deny Beep 02 .wav" Type="Document" URL="../Sounds/Deny Beep 02 .wav"/>
				<Item Name="Deny Beep 03 .wav" Type="Document" URL="../Sounds/Deny Beep 03 .wav"/>
				<Item Name="Diagnostic Complete.wav" Type="Document" URL="../Sounds/Diagnostic Complete.wav"/>
				<Item Name="Diagnostic Cycle Will Be Complete.wav" Type="Document" URL="../Sounds/Diagnostic Cycle Will Be Complete.wav"/>
				<Item Name="Direction Unclear.wav" Type="Document" URL="../Sounds/Direction Unclear.wav"/>
				<Item Name="Distress Signal .wav" Type="Document" URL="../Sounds/Distress Signal .wav"/>
				<Item Name="ds9start.wav" Type="Document" URL="../Sounds/ds9start.wav"/>
				<Item Name="Emergency Power Engaged.wav" Type="Document" URL="../Sounds/Emergency Power Engaged.wav"/>
				<Item Name="Emission is Not Consistant.wav" Type="Document" URL="../Sounds/Emission is Not Consistant.wav"/>
				<Item Name="empty.wav" Type="Document" URL="../Sounds/empty.wav"/>
				<Item Name="End Simulation.wav" Type="Document" URL="../Sounds/End Simulation.wav"/>
				<Item Name="Energy Field Stregnth Increasing.wav" Type="Document" URL="../Sounds/Energy Field Stregnth Increasing.wav"/>
				<Item Name="Energy Reserves Critical State.wav" Type="Document" URL="../Sounds/Energy Reserves Critical State.wav"/>
				<Item Name="Enter Authorization Code.wav" Type="Document" URL="../Sounds/Enter Authorization Code.wav"/>
				<Item Name="Enter When Ready.wav" Type="Document" URL="../Sounds/Enter When Ready.wav"/>
				<Item Name="error.wav" Type="Document" URL="../Sounds/error.wav"/>
				<Item Name="exclamation.wav" Type="Document" URL="../Sounds/exclamation.wav"/>
				<Item Name="Executed.wav" Type="Document" URL="../Sounds/Executed.wav"/>
				<Item Name="exit.wav" Type="Document" URL="../Sounds/exit.wav"/>
				<Item Name="Hail Alert 01 .wav" Type="Document" URL="../Sounds/Hail Alert 01 .wav"/>
				<Item Name="Hail Alert 02 .wav" Type="Document" URL="../Sounds/Hail Alert 02 .wav"/>
				<Item Name="Hailing Frequencies Open .wav" Type="Document" URL="../Sounds/Hailing Frequencies Open .wav"/>
				<Item Name="Holodeck 3 Program is Re-enststed.wav" Type="Document" URL="../Sounds/Holodeck 3 Program is Re-enststed.wav"/>
				<Item Name="Holodeck 3 Program is Ready.wav" Type="Document" URL="../Sounds/Holodeck 3 Program is Ready.wav"/>
				<Item Name="Incoming Transmission.wav" Type="Document" URL="../Sounds/Incoming Transmission.wav"/>
				<Item Name="Information on this Sector.wav" Type="Document" URL="../Sounds/Information on this Sector.wav"/>
				<Item Name="Initiating Update.wav" Type="Document" URL="../Sounds/Initiating Update.wav"/>
				<Item Name="Input Algorithm Not Accepted.wav" Type="Document" URL="../Sounds/Input Algorithm Not Accepted.wav"/>
				<Item Name="Input Received.wav" Type="Document" URL="../Sounds/Input Received.wav"/>
				<Item Name="Inquires are No Longer Accepted.wav" Type="Document" URL="../Sounds/Inquires are No Longer Accepted.wav"/>
				<Item Name="Insufficient Data.wav" Type="Document" URL="../Sounds/Insufficient Data.wav"/>
				<Item Name="Interface Complete.wav" Type="Document" URL="../Sounds/Interface Complete.wav"/>
				<Item Name="intro.wav" Type="Document" URL="../Sounds/intro.wav"/>
				<Item Name="Link Cancelled.wav" Type="Document" URL="../Sounds/Link Cancelled.wav"/>
				<Item Name="Link Data Disconnect.wav" Type="Document" URL="../Sounds/Link Data Disconnect.wav"/>
				<Item Name="Linkage Complete.wav" Type="Document" URL="../Sounds/Linkage Complete.wav"/>
				<Item Name="Logs Accessed.wav" Type="Document" URL="../Sounds/Logs Accessed.wav"/>
				<Item Name="maximize.wav" Type="Document" URL="../Sounds/maximize.wav"/>
				<Item Name="Message Transmitted.wav" Type="Document" URL="../Sounds/Message Transmitted.wav"/>
				<Item Name="minimize.wav" Type="Document" URL="../Sounds/minimize.wav"/>
				<Item Name="Negative.wav" Type="Document" URL="../Sounds/Negative.wav"/>
				<Item Name="No Audio Anomolies Present.wav" Type="Document" URL="../Sounds/No Audio Anomolies Present.wav"/>
				<Item Name="No Introferalmetric Patterns.wav" Type="Document" URL="../Sounds/No Introferalmetric Patterns.wav"/>
				<Item Name="No Subspace Projections Present.wav" Type="Document" URL="../Sounds/No Subspace Projections Present.wav"/>
				<Item Name="No Temporal Anomolys.wav" Type="Document" URL="../Sounds/No Temporal Anomolys.wav"/>
				<Item Name="No Z Particles Present.wav" Type="Document" URL="../Sounds/No Z Particles Present.wav"/>
				<Item Name="Now Disconnected.wav" Type="Document" URL="../Sounds/Now Disconnected.wav"/>
				<Item Name="Now Establishing Datalink.wav" Type="Document" URL="../Sounds/Now Establishing Datalink.wav"/>
				<Item Name="One Minute to Auto-Destruct.wav" Type="Document" URL="../Sounds/One Minute to Auto-Destruct.wav"/>
				<Item Name="Online Assistance Activated.wav" Type="Document" URL="../Sounds/Online Assistance Activated.wav"/>
				<Item Name="Open Hailing Frequencies .wav" Type="Document" URL="../Sounds/Open Hailing Frequencies .wav"/>
				<Item Name="open.wav" Type="Document" URL="../Sounds/open.wav"/>
				<Item Name="Override Authority Restricted.wav" Type="Document" URL="../Sounds/Override Authority Restricted.wav"/>
				<Item Name="Panel Beep 01 .wav" Type="Document" URL="../Sounds/Panel Beep 01 .wav"/>
				<Item Name="Panel Beep 02 .wav" Type="Document" URL="../Sounds/Panel Beep 02 .wav"/>
				<Item Name="Panel Beep 03 .wav" Type="Document" URL="../Sounds/Panel Beep 03 .wav"/>
				<Item Name="Panel Beep 04 .wav" Type="Document" URL="../Sounds/Panel Beep 04 .wav"/>
				<Item Name="Panel Beep 05 .wav" Type="Document" URL="../Sounds/Panel Beep 05 .wav"/>
				<Item Name="Panel Beep 06 .wav" Type="Document" URL="../Sounds/Panel Beep 06 .wav"/>
				<Item Name="Panel Beep 07 .wav" Type="Document" URL="../Sounds/Panel Beep 07 .wav"/>
				<Item Name="Panel Beep 08 .wav" Type="Document" URL="../Sounds/Panel Beep 08 .wav"/>
				<Item Name="Panel Beep 09 .wav" Type="Document" URL="../Sounds/Panel Beep 09 .wav"/>
				<Item Name="Panel Beep 10 .wav" Type="Document" URL="../Sounds/Panel Beep 10 .wav"/>
				<Item Name="Panel Beep 11 .wav" Type="Document" URL="../Sounds/Panel Beep 11 .wav"/>
				<Item Name="Panel Beep 12 .wav" Type="Document" URL="../Sounds/Panel Beep 12 .wav"/>
				<Item Name="Panel Beep 13 .wav" Type="Document" URL="../Sounds/Panel Beep 13 .wav"/>
				<Item Name="Panel Beep 14 .wav" Type="Document" URL="../Sounds/Panel Beep 14 .wav"/>
				<Item Name="Panel Beep 15 .wav" Type="Document" URL="../Sounds/Panel Beep 15 .wav"/>
				<Item Name="Panel Beep 16.wav" Type="Document" URL="../Sounds/Panel Beep 16.wav"/>
				<Item Name="Panel Beep 17 .wav" Type="Document" URL="../Sounds/Panel Beep 17 .wav"/>
				<Item Name="Panel Beep 18 .wav" Type="Document" URL="../Sounds/Panel Beep 18 .wav"/>
				<Item Name="Panel Beep 19 .wav" Type="Document" URL="../Sounds/Panel Beep 19 .wav"/>
				<Item Name="Panel Beep 20 .wav" Type="Document" URL="../Sounds/Panel Beep 20 .wav"/>
				<Item Name="Please Confirm Deactivation Request.wav" Type="Document" URL="../Sounds/Please Confirm Deactivation Request.wav"/>
				<Item Name="Please Confirm Deactivation.wav" Type="Document" URL="../Sounds/Please Confirm Deactivation.wav"/>
				<Item Name="Please Do Not Address This Unit.wav" Type="Document" URL="../Sounds/Please Do Not Address This Unit.wav"/>
				<Item Name="Please Enter Program.wav" Type="Document" URL="../Sounds/Please Enter Program.wav"/>
				<Item Name="Please Indicate to Over-ride.wav" Type="Document" URL="../Sounds/Please Indicate to Over-ride.wav"/>
				<Item Name="Please Input Command Codes.wav" Type="Document" URL="../Sounds/Please Input Command Codes.wav"/>
				<Item Name="Please Repeat Request.wav" Type="Document" URL="../Sounds/Please Repeat Request.wav"/>
				<Item Name="Please Restate a Single Question.wav" Type="Document" URL="../Sounds/Please Restate a Single Question.wav"/>
				<Item Name="Please Restate Question.wav" Type="Document" URL="../Sounds/Please Restate Question.wav"/>
				<Item Name="Please Restate Request.wav" Type="Document" URL="../Sounds/Please Restate Request.wav"/>
				<Item Name="Please Specify How.wav" Type="Document" URL="../Sounds/Please Specify How.wav"/>
				<Item Name="Please Specify.wav" Type="Document" URL="../Sounds/Please Specify.wav"/>
				<Item Name="popup.wav" Type="Document" URL="../Sounds/popup.wav"/>
				<Item Name="Priority Clearance Alpha One.wav" Type="Document" URL="../Sounds/Priority Clearance Alpha One.wav"/>
				<Item Name="Priority Message.wav" Type="Document" URL="../Sounds/Priority Message.wav"/>
				<Item Name="Priority One Distress Signal.wav" Type="Document" URL="../Sounds/Priority One Distress Signal.wav"/>
				<Item Name="Priority One Message.wav" Type="Document" URL="../Sounds/Priority One Message.wav"/>
				<Item Name="Program Alpha One.wav" Type="Document" URL="../Sounds/Program Alpha One.wav"/>
				<Item Name="Program Complete, Enter.wav" Type="Document" URL="../Sounds/Program Complete, Enter.wav"/>
				<Item Name="Program Complete.wav" Type="Document" URL="../Sounds/Program Complete.wav"/>
				<Item Name="Program in Progress.wav" Type="Document" URL="../Sounds/Program in Progress.wav"/>
				<Item Name="Program Loaded and Ready.wav" Type="Document" URL="../Sounds/Program Loaded and Ready.wav"/>
				<Item Name="Provide Requested Data.wav" Type="Document" URL="../Sounds/Provide Requested Data.wav"/>
				<Item Name="question.wav" Type="Document" URL="../Sounds/question.wav"/>
				<Item Name="Re-establishing Link.wav" Type="Document" URL="../Sounds/Re-establishing Link.wav"/>
				<Item Name="Ready.wav" Type="Document" URL="../Sounds/Ready.wav"/>
				<Item Name="Relaod Circuits are Initializing.wav" Type="Document" URL="../Sounds/Relaod Circuits are Initializing.wav"/>
				<Item Name="reminder.wav" Type="Document" URL="../Sounds/reminder.wav"/>
				<Item Name="Repeat Command.wav" Type="Document" URL="../Sounds/Repeat Command.wav"/>
				<Item Name="restoredown.wav" Type="Document" URL="../Sounds/restoredown.wav"/>
				<Item Name="restoreup.wav" Type="Document" URL="../Sounds/restoreup.wav"/>
				<Item Name="ringin.wav" Type="Document" URL="../Sounds/ringin.wav"/>
				<Item Name="ringout.wav" Type="Document" URL="../Sounds/ringout.wav"/>
				<Item Name="Search Mode Activated.wav" Type="Document" URL="../Sounds/Search Mode Activated.wav"/>
				<Item Name="Searching.wav" Type="Document" URL="../Sounds/Searching.wav"/>
				<Item Name="Security Authorization Accepted.wav" Type="Document" URL="../Sounds/Security Authorization Accepted.wav"/>
				<Item Name="Security Clearance Required.wav" Type="Document" URL="../Sounds/Security Clearance Required.wav"/>
				<Item Name="Security Level 3 Is Required.wav" Type="Document" URL="../Sounds/Security Level 3 Is Required.wav"/>
				<Item Name="Select Button.wav" Type="Document" URL="../Sounds/Select Button.wav"/>
				<Item Name="Select Exit Help Button.wav" Type="Document" URL="../Sounds/Select Exit Help Button.wav"/>
				<Item Name="Select Menu.wav" Type="Document" URL="../Sounds/Select Menu.wav"/>
				<Item Name="Selected Word Not Found.wav" Type="Document" URL="../Sounds/Selected Word Not Found.wav"/>
				<Item Name="Shied Penetration 17%.wav" Type="Document" URL="../Sounds/Shied Penetration 17%.wav"/>
				<Item Name="Specify Geographical Regions.wav" Type="Document" URL="../Sounds/Specify Geographical Regions.wav"/>
				<Item Name="Specify Parameters.wav" Type="Document" URL="../Sounds/Specify Parameters.wav"/>
				<Item Name="Specify Search Parameters.wav" Type="Document" URL="../Sounds/Specify Search Parameters.wav"/>
				<Item Name="Standard Procedure Requires.wav" Type="Document" URL="../Sounds/Standard Procedure Requires.wav"/>
				<Item Name="Starfleet Academy.wav" Type="Document" URL="../Sounds/Starfleet Academy.wav"/>
				<Item Name="Starfleet Headquarters.wav" Type="Document" URL="../Sounds/Starfleet Headquarters.wav"/>
				<Item Name="Starfleet Science Institute.wav" Type="Document" URL="../Sounds/Starfleet Science Institute.wav"/>
				<Item Name="start.wav" Type="Document" URL="../Sounds/start.wav"/>
				<Item Name="systemdefault.wav" Type="Document" URL="../Sounds/systemdefault.wav"/>
				<Item Name="That Has Not Been Programed.wav" Type="Document" URL="../Sounds/That Has Not Been Programed.wav"/>
				<Item Name="That Info Is Not Available.wav" Type="Document" URL="../Sounds/That Info Is Not Available.wav"/>
				<Item Name="That Information is Classified.wav" Type="Document" URL="../Sounds/That Information is Classified.wav"/>
				<Item Name="That is Not a Valid Question.wav" Type="Document" URL="../Sounds/That is Not a Valid Question.wav"/>
				<Item Name="That Program is Already in Use.wav" Type="Document" URL="../Sounds/That Program is Already in Use.wav"/>
				<Item Name="That Program is Available.wav" Type="Document" URL="../Sounds/That Program is Available.wav"/>
				<Item Name="The Automatic Pilot is Not Fuctional.wav" Type="Document" URL="../Sounds/The Automatic Pilot is Not Fuctional.wav"/>
				<Item Name="The Holo-Matrix Has Been Formated.wav" Type="Document" URL="../Sounds/The Holo-Matrix Has Been Formated.wav"/>
				<Item Name="The Logs Have Been Deleted.wav" Type="Document" URL="../Sounds/The Logs Have Been Deleted.wav"/>
				<Item Name="The Program Has Been Reinitiated.wav" Type="Document" URL="../Sounds/The Program Has Been Reinitiated.wav"/>
				<Item Name="This Terminal is Not a Replicator.wav" Type="Document" URL="../Sounds/This Terminal is Not a Replicator.wav"/>
				<Item Name="Transfer Complete.wav" Type="Document" URL="../Sounds/Transfer Complete.wav"/>
				<Item Name="Transmit Message.wav" Type="Document" URL="../Sounds/Transmit Message.wav"/>
				<Item Name="Unable to Comply.wav" Type="Document" URL="../Sounds/Unable to Comply.wav"/>
				<Item Name="Unknown.wav" Type="Document" URL="../Sounds/Unknown.wav"/>
				<Item Name="Update Complete.wav" Type="Document" URL="../Sounds/Update Complete.wav"/>
				<Item Name="voystart.wav" Type="Document" URL="../Sounds/voystart.wav"/>
				<Item Name="Warning Warp Core Collapse.wav" Type="Document" URL="../Sounds/Warning Warp Core Collapse.wav"/>
				<Item Name="Warning.wav" Type="Document" URL="../Sounds/Warning.wav"/>
				<Item Name="Warp Energy Increase.wav" Type="Document" URL="../Sounds/Warp Energy Increase.wav"/>
				<Item Name="Welcome to Starfleet Academy.wav" Type="Document" URL="../Sounds/Welcome to Starfleet Academy.wav"/>
				<Item Name="Welcome to Starfleet Command.wav" Type="Document" URL="../Sounds/Welcome to Starfleet Command.wav"/>
				<Item Name="Welcome to Starfleet Science Institute.wav" Type="Document" URL="../Sounds/Welcome to Starfleet Science Institute.wav"/>
				<Item Name="Working.wav" Type="Document" URL="../Sounds/Working.wav"/>
				<Item Name="You Are not Authorized.wav" Type="Document" URL="../Sounds/You Are not Authorized.wav"/>
				<Item Name="Your Request Does Not Guidelines.wav" Type="Document" URL="../Sounds/Your Request Does Not Guidelines.wav"/>
				<Item Name="Zimmerman Program Complete.wav" Type="Document" URL="../Sounds/Zimmerman Program Complete.wav"/>
			</Item>
			<Item Name="LabVIEW Speak TNG.ico" Type="Document" URL="../LVSpeak TNG/LabVIEW Speak TNG.ico"/>
			<Item Name="LabVIEW Speak TNG2.ico" Type="Document" URL="../LVSpeak TNG/LabVIEW Speak TNG2.ico"/>
			<Item Name="LVSpeak TNG.lvclass" Type="LVClass" URL="../LVSpeak TNG/LVSpeak TNG.lvclass"/>
		</Item>
		<Item Name="NotifyIcon" Type="Folder">
			<Item Name="NotifyIcon.lvclass" Type="LVClass" URL="../NotifyIcon/NotifyIcon.lvclass"/>
		</Item>
		<Item Name="Presentation" Type="Folder">
			<Item Name="Chicago LabVIEW Users Group Q2-2021.lvclass" Type="LVClass" URL="../../Presentations/Chicago LabVIEW Users Group Q2-2021/Chicago LabVIEW Users Group Q2-2021.lvclass"/>
			<Item Name="LabVIEW Project Demo.lvclass" Type="LVClass" URL="../../PowerPoint Controller/LabVIEW Project Demo/LabVIEW Project Demo.lvclass"/>
			<Item Name="PowerPoint Presentation Commands.lvclass" Type="LVClass" URL="../Command Sets/PowerPoint Presentation Commands/PowerPoint Presentation Commands.lvclass"/>
			<Item Name="PowerPointController.lvlib" Type="Library" URL="../../PowerPoint Controller/PowerPointController.lvlib"/>
		</Item>
		<Item Name="Speech Recognition Engine" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="SRE.lvclass" Type="LVClass" URL="../Speech Recognition Engine/SRE.lvclass"/>
		</Item>
		<Item Name="Windows Integration" Type="Folder">
			<Item Name="Windows Integration.lvclass" Type="LVClass" URL="../../Windows Integration/Windows Integration.lvclass"/>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="_2DArrToArrWfms.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/_2DArrToArrWfms.vi"/>
				<Item Name="_ArrWfmsTo1DInterleave.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/_ArrWfmsTo1DInterleave.vi"/>
				<Item Name="_ArrWfmsTo2DArr.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/_ArrWfmsTo2DArr.vi"/>
				<Item Name="_ArrWfmsToData.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/_ArrWfmsToData.vi"/>
				<Item Name="_Get Sound Error From Return Value.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/_Get Sound Error From Return Value.vi"/>
				<Item Name="Acquire Input Data.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Acquire Input Data.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close Input Device.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Close Input Device.vi"/>
				<Item Name="closeJoystick.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/closeJoystick.vi"/>
				<Item Name="closeKeyboard.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/closeKeyboard.vi"/>
				<Item Name="closeMouse.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/closeMouse.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Equal Comparable.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Comparison/Equal/Equal Comparable/Equal Comparable.lvclass"/>
				<Item Name="Equal Functor.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Comparison/Equal/Equal Functor/Equal Functor.lvclass"/>
				<Item Name="Equals.vim" Type="VI" URL="/&lt;vilib&gt;/Comparison/Equals.vim"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrorDescriptions.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/ErrorDescriptions.vi"/>
				<Item Name="errorList.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/errorList.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Intialize Keyboard.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Intialize Keyboard.vi"/>
				<Item Name="joystickAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/joystickAcquire.vi"/>
				<Item Name="keyboardAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/keyboardAcquire.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="mouseAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/mouseAcquire.vi"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Path To Command Line String.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Path To Command Line String.vi"/>
				<Item Name="PathToUNIXPathString.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/PathToUNIXPathString.vi"/>
				<Item Name="Play Sound File.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Play Sound File.vi"/>
				<Item Name="Random Number (Range) DBL.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range) DBL.vi"/>
				<Item Name="Random Number (Range) I64.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range) I64.vi"/>
				<Item Name="Random Number (Range) U64.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range) U64.vi"/>
				<Item Name="Random Number (Range).vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range).vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Sampling Mode.ctl" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sampling Mode.ctl"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Search Unsorted 1D Array Core.vim" Type="VI" URL="/&lt;vilib&gt;/Array/Helpers/Search Unsorted 1D Array Core.vim"/>
				<Item Name="Search Unsorted 1D Array.vim" Type="VI" URL="/&lt;vilib&gt;/Array/Search Unsorted 1D Array.vim"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Sound Data Format.ctl" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Data Format.ctl"/>
				<Item Name="Sound File Close.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Close.vi"/>
				<Item Name="Sound File Info (path).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Info (path).vi"/>
				<Item Name="Sound File Info (refnum).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Info (refnum).vi"/>
				<Item Name="Sound File Info.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Info.vi"/>
				<Item Name="Sound File Open.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Open.vi"/>
				<Item Name="Sound File Position.ctl" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Position.ctl"/>
				<Item Name="Sound File Read (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read (DBL).vi"/>
				<Item Name="Sound File Read (I16).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read (I16).vi"/>
				<Item Name="Sound File Read (I32).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read (I32).vi"/>
				<Item Name="Sound File Read (SGL).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read (SGL).vi"/>
				<Item Name="Sound File Read (U8).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read (U8).vi"/>
				<Item Name="Sound File Read Open.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read Open.vi"/>
				<Item Name="Sound File Read.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read.vi"/>
				<Item Name="Sound File Refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Refnum.ctl"/>
				<Item Name="Sound File Write Open.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Write Open.vi"/>
				<Item Name="Sound Output Configure.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Configure.vi"/>
				<Item Name="Sound Output Task ID.ctl" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Task ID.ctl"/>
				<Item Name="Sound Output Wait.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Wait.vi"/>
				<Item Name="Sound Output Write (DBL Single).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Write (DBL Single).vi"/>
				<Item Name="Sound Output Write (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Write (DBL).vi"/>
				<Item Name="Sound Output Write (I16).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Write (I16).vi"/>
				<Item Name="Sound Output Write (I32).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Write (I32).vi"/>
				<Item Name="Sound Output Write (SGL).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Write (SGL).vi"/>
				<Item Name="Sound Output Write (U8).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Write (U8).vi"/>
				<Item Name="Sound Output Write.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Write.vi"/>
				<Item Name="sub_Random U32.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/sub_Random U32.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="lvinput.dll" Type="Document" URL="/&lt;resource&gt;/lvinput.dll"/>
			<Item Name="lvsound2.dll" Type="Document" URL="/&lt;resource&gt;/lvsound2.dll"/>
			<Item Name="mscorlib" Type="VI" URL="mscorlib">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="System.Drawing" Type="Document" URL="System.Drawing">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="System.Speech" Type="Document" URL="System.Speech">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="System.Windows.Forms" Type="Document" URL="System.Windows.Forms">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="user32.dll" Type="Document" URL="user32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Zipped Source Distribution" Type="Zip File">
				<Property Name="Absolute[0]" Type="Bool">false</Property>
				<Property Name="BuildName" Type="Str">Zipped Source Distribution</Property>
				<Property Name="Comments" Type="Str"></Property>
				<Property Name="DestinationID[0]" Type="Str">{AF1B4873-305C-47F1-907E-BA19F3C41A90}</Property>
				<Property Name="DestinationItemCount" Type="Int">1</Property>
				<Property Name="DestinationName[0]" Type="Str">Destination Directory</Property>
				<Property Name="IncludedItemCount" Type="Int">1</Property>
				<Property Name="IncludedItems[0]" Type="Ref">/My Computer</Property>
				<Property Name="IncludeProject" Type="Bool">true</Property>
				<Property Name="Path[0]" Type="Path">../../Builds/LabVIEW Speak/LabVIEW Speak.zip</Property>
				<Property Name="ZipBase" Type="Str">NI_zipbasedefault</Property>
			</Item>
		</Item>
	</Item>
</Project>
